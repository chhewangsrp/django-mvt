from django.test import TestCase


class TestViews(TestCase):
    """
    Test the views
    """

    def test_get_home_page(self):
        """
        Test the home page
        """
        page = self.client.get("/")
        self.assertEqual(page.status_code, 200)
        self.assertTemplateUsed(page, "core/index.html")
