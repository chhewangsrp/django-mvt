from django.shortcuts import render

# Create your views here.


def home(request):
    """Renders the home page."""
    context = {}
    return render(request, "core/index.html", context)


def register(request):
    """Renders the register page."""

    context = {"register": "Welcome to the register page!"}
    return render(request, "core/register.html", context)
