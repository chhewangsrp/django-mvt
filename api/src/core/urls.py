from core import views
from django.urls import path

urlpatterns = [
    path("", views.base.home, name="home"),
    path("register", views.base.register, name="register"),
]
