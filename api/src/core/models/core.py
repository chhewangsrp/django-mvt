# core/models/common.py

from django.db import models
from django.utils import timezone


class BaseModel(models.Model):
    """
    An abstract base class model that provides self-updating
    'created' and 'updated' fields.
    """

    created_at = models.DateTimeField(default=timezone.now, editable=False)
    updated_at = models.DateTimeField(default=timezone.now)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        Update 'updated_at' field every time the model is saved.
        """
        self.updated_at = timezone.now()
        super().save(*args, **kwargs)


# Other shared functionalities, utilities, or constants can be defined here
