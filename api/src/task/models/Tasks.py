from django.db import models
from django.contrib.auth.models import User

from core.models import BaseModel


class Task(BaseModel):
    """Task model

    A task includes:
    - id: primary key
    - assigned_to: foreign key to User
    - created_by: foreign key to User
    - title: string
    - description: text
    - due_date: datetime
    - status: string
    - priority: string
    - created_at: datetime
    - updated_at: datetime

    """

    STATUS_CHOICES = [
        ("pending", "Pending"),
        ("in_progress", "In Progress"),
        ("completed", "Completed"),
    ]

    PRIORITY_CHOICES = [
        ("low", "Low"),
        ("medium", "Medium"),
        ("high", "High"),
    ]

    assigned_to = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE
    )
    created_by = models.ForeignKey(
        User, related_name="created_tasks", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=200)
    description = models.TextField()
    due_date = models.DateTimeField()
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default="pending")
    priority = models.CharField(
        max_length=20, choices=PRIORITY_CHOICES, default="medium"
    )

    @property
    def id(self) -> int:
        "Primary key of the task"
        return self.id
