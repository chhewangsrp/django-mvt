from django.contrib.auth.models import User
from django.db import models

from core.models import BaseModel


class Project(BaseModel):
    """Project model

    A project includes:
    - id: primary key
    - name: project name
    - description: project description
    - created_at: project creation time
    - updated_at: project update time

    """

    project_manager = models.ForeignKey(
        User, to_field="username", on_delete=models.CASCADE
    )
    members = models.ManyToManyField(User, related_name="members")
    name = models.CharField(max_length=100)
    description = models.TextField()

    @property
    def id(self) -> int:
        "Primary key of the project"
        return self.id
