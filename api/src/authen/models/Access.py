from django.contrib.auth.models import User
from django.db import models

from core.models import BaseModel


class Access(BaseModel):
    """Access model

    An access includes:
    - id: primary key
    - user: user who has access
    - role: role of user
    - created_at: time of creation
    - updated_at: time of last update
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey("authen.Role", on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    @property
    def id(self) -> int:
        "Primary key of the access"
        return self.id
