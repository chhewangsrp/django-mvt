from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save


class Profile(models.Model):
    """Profile model

    A profile includes:
    - id: primary key
    - user: foreign key that references the user table
    - title: title of the profile
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)

    @property
    def id(self) -> int:
        "Primary key of the profile"
        return self.id


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     """Create a profile when a user is created"""
#     if created:
#         Profile.objects.create(user=instance)


# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     """Save the profile of a user"""
#     instance.profile.save()
