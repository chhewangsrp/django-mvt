from django.db import models
from django.core.validators import MinValueValidator

from core.models import BaseModel


class Role(BaseModel):
    """Role model

    A role includes:
    - id: primary key
    - name: role name
    - role_description: role description
    - created_at: role created time
    - updated_at: role updated time
    """

    role_name = models.CharField(max_length=512)
    role_description = models.CharField(max_length=512)
    role_level = models.IntegerField(unique=True, validators=[MinValueValidator(1)])

    def id(self) -> int:
        "Primary key of the role"
        return self.id

    class Meta:
        ordering = ["role_level"]


class RoleLevels:
    """Role levels"""

    ADMIN = 1
    PROJECT_MANAGER = 2
    TASK_MANAGER = 3
    DEVELOPER = 4
