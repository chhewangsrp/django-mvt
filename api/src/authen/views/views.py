from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from authen.forms import UserForm, ProfileForm

# Create your views here.


@require_http_methods(["POST"])
def register(request):
    """Registration view for taskify"""

    user_form = UserForm(data=request.POST)
    profile_form = ProfileForm(data=request.POST)

    if user_form.is_valid() and profile_form.is_valid():
        user = user_form.save()
        profile = profile_form.save(commit=False)
        profile.user = user
        profile.save()
        return render(request, "registration/register_done.html", {"user": user})

    return render(
        request,
        "registration/register.html",
        {"user_form": user_form, "profile_form": profile_form},
    )
