from django.contrib.auth.models import User
from django.forms import ModelForm
from authen.models.Profile import Profile


class UserForm(ModelForm):
    """User form"""

    class Meta:
        """Meta class for User forms"""

        model = User
        fields = ["username", "email", "password1", "password2"]


class ProfileForm(ModelForm):
    """Profile form"""

    class Meta:
        """Meta class for User profile"""

        model = Profile
        fields = "__all__"
